<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('users.login');
});
//

// Confide routes
Route::get('users/create', 'UsersController@create');
Route::post('users/store', 'UsersController@store');
Route::post('user/storeuser','UsersController@storeuser');

Route::get('users/login', 'UsersController@login');
Route::post('users/login', 'UsersController@doLogin');
Route::get('users/confirm/{code}', 'UsersController@confirm');
Route::get('users/forgot_password', 'UsersController@forgotPassword');
Route::post('users/forgot_password', 'UsersController@doForgotPassword');
Route::get('users/reset_password/{token}', 'UsersController@resetPassword');
Route::post('users/reset_password', 'UsersController@doResetPassword');
Route::get('users/logout', 'UsersController@logout');
Route::Resource('dashboard', 'DashboardController@index');
Route::Resource('emulator', 'UsersController@emulator');

Route::get('invitations', 'InvitationsController@index');
Route::get('users', 'UsersController@index');

Route::get('companies','CompaniesController@index');

Route::get('sacco/create','CompaniesController@create');
Route::post('sacco/store','CompaniesController@storeSacco');
Route::get('sacco/view/{id}','CompaniesController@show');

//user creation
Route::get('member/create/{id}','SaccomemberController@create');
Route::post('member/store','SaccomemberController@storemember');
//Route::get('member/view/{id}','SaccomemberController@showmember');

Route::get('issues','IssuesController@index');
Route::post('issues/create','IssuesController@create');

Route::Resource('ussd', 'UssdController@index');
Route::Resource('testloan', 'UssdController@testloan');
Route::get('ussd/enr', 'UssdController@sendReportToENR');
//Route::Resource('ussd/menu_items/{menu_id}', 'UssdController@getMenuItems');
Route::get('company/{company_id}', 'CompaniesController@company');
Route::Resource('company', 'CompaniesController@store');
Route::Resource('employee/store','CompaniesController@storeemployee');
Route::get('ussd/get_menu/{menu_id}', 'UssdController@getMenu');
Route::get('ussd/menu', 'UssdController@getMainMenu');
Route::get('ussd/sendmoney', 'UssdController@sendmoney');


//menu

//Route::get('/ussd', function(){
//	echo "It works";
//});


