@extends('layouts.dashlayout',['role' => $role])
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Dashboard <small>statistics & reports</small></h1>
                </div>
                <!-- END PAGE TITLE -->

            </div>
        </div>
    <div class="page-content">
        <div class="container">
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb breadcrumb hide">
                <li>
                    <a href="#">Home</a><i class="fa fa-circle"></i>
                </li>
                <li class="active">
                    Dashboard
                </li>
            </ul>
            <!-- END PAGE BREADCRUMB -->
            <!-- BEGIN PAGE CONTENT INNER -->
            <div class="row margin-top-10">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <a class="dashboard-stat dashboard-stat-light blue-soft" href="#">
                        <div class="visual">
                            <i class="fa fa-users"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{count($saccos)}}
                            </div>
                            <div class="desc">
                                Number of Saccos
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <a class="dashboard-stat dashboard-stat-light purple-soft" href="#">
                        <div class="visual">
                            <i class="fa fa-briefcase"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{count($users)}}
                            </div>
                            <div class="desc">
                                Total Number of Users
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <a class="dashboard-stat dashboard-stat-light green-soft" href="#">
                        <div class="visual">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                               0
                            </div>
                            <div class="desc">
                                Total Loans Disbursed
                            </div>
                        </div>
                    </a>
                </div>
                {{--<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">--}}
                    {{--<a class="dashboard-stat dashboard-stat-light blue-soft" href="#">--}}
                        {{--<div class="visual">--}}
                            {{--<i class="fa fa-users"></i>--}}
                        {{--</div>--}}
                        {{--<div class="details">--}}
                            {{--<div class="number">--}}
                                {{--0--}}
                            {{--</div>--}}
                            {{--<div class="desc">--}}
                                {{--Margins--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                {{--</div>--}}

            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    <div class="portlet light">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-briefcase font-green-sharp"></i>
                                <span class="caption-subject font-green-sharp bold uppercase">Saccos</span>
                            </div>
                            <div class="caption pull-right">
                                <i class="fa fa-plus font-blue-sharp"></i>
                                <a href="{{url('/sacco/create')}}"><span class="caption-subject font-blue-sharp bold uppercase">Add Sacco</span></a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-scrollable">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                    <tr>
                                        <th>
                                            <i class="fa fa-briefcase"></i> Sacco
                                        </th>
                                        <th class="hidden-xs">
                                            <i class="fa fa-user"></i> Number of Users
                                        </th>
                                        <th>
                                            <i class="fa fa-money"></i> Total Loan Taken
                                        </th>
                                        <th>
                                            <i class="fa fa-wrench"></i> Actions
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($saccos as $sacco)
                                    <tr>
                                        <td class="highlight">
                                            <div class="success">
                                            </div>
                                            <a href="{{url('sacco/view',$sacco->id)}}">
                                                {{$sacco->name}}</a>
                                        </td>
                                        <td class="hidden-xs">
                                            0
                                        </td>
                                        <td>
                                            0
                                        </td>
                                        <td>
                                            <a href="javascript:;" class="btn btn-xs btn-success">
                                                <i class="fa fa-edit"></i> Edit </a>
                                            <a href="javascript:;" class="btn default btn-xs red">
                                                <i class="fa fa-trash-o"></i> Delete </a>
                                        </td>
                                    </tr>
                                    @endforeach



                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->
                </div>
            </div>
            <!-- END PAGE CONTENT INNER -->
        </div>
    </div>
    @stop