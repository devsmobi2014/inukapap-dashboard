@extends('users.layouts.user')

@section('content')
<div class="container">
    <div class="row">
        <br>
        <div class="col-md-4 col-md-offset-4">
            <hr />
            <div class="well">
                <form method="post" action="{{ URL::to('users/login') }}" >

                    <h1>Log in </h1>

                    <div class="email" style="color:black;">Username/Email:</div>
                    <input name="email" type="text" class="field form-control" id="email"/>
                    <br />

                    <div class="label" style="color:black;">Password:</div>
                    <input name="password" type="password" class="field form-control" id="password"/>
                    <br />

                    <input name="submit" type="submit" class="btn btn-info btn-block" value="Login up " />

                    <br />
                    <br />
                    <!-- <div class="center">
                    <a href="forgotpassword.php">Forgot Password?</a> | <a href="register.php">New User?</a>
                    </div> -->

                </form>


            </div>
            @if ( Session::get('error') )
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif

            @if ( Session::get('notice') )
                <div class="alert">
                    {{ Session::get('notice') }}
                </div>
            @endif
            <div class="forget-password">
                <p>
                    Forget password? no worries, click <a  href="{{URL::to('users/forgot_password')}}" id="forget-password">{{ Lang::get('confide::confide.login.forgot_password') }}</a>
                    to reset your password.
                </p>
            </div>
        </div>
    </div>
</div>

 @stop