<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/ico/favicon.png">

    <title>Dashboard</title>

    <!-- Bootstrap core CSS -->
    <link href="/dist/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/dist/css/starter-template.css" rel="stylesheet">
    <link href="/dist/css/justified-nav.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="../../assets/js/html5shiv.js"></script>
    <script src="../../assets/js/respond.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

    <script>
        $(function () {
            $('#container').highcharts({
                chart: {
                    type: 'spline'
                },
                title: {
                    text: 'Transactions'
                },
                subtitle: {
                    text: 'Tabulated per hour'
                },
                xAxis: {
                    title: {
                        text: 'Time'
                    },
                    categories: ['7am', '8am', '9am', '10am', '11am', '12am',
                        '1pm', '2pm', '3pm', '4pm', '5pm', '6pm']
                },
                yAxis: {
                    title: {
                        text: 'No of Transactions'
                    },
                    labels: {
                        formatter: function() {
                            return this.value +''
                        }
                    }
                },
                tooltip: {
                    crosshairs: true,
                    shared: true
                },
                plotOptions: {
                    spline: {
                        marker: {
                            radius: 4,
                            lineColor: '#666666',
                            lineWidth: 1
                        }
                    }
                },
                series: [{
                    name: 'Transactions',
                    marker: {
                        symbol: 'square'
                    },
                    data: [0.0, 4.0, 6.0, 10.0, 11.0,  {
                        y: 12.0,
                        marker: {
                            symbol: 'url(http://www.highcharts.com/demo/gfx/sun.png)'
                        }
                    },5.0, 7.0, 5.0]

                }]
            });
        });


    </script>
    <![endif]-->
</head>

<body>

<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Bulk Airtime</a>
        </div>
        <div class="collapse navbar-collapse pull-right">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Welcome back</a></li>
                <li class="pull-right"><a href="/users/logout">Log out</a></li>
                {{--<li><a href="#contact">Contact</a></li>--}}
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>


<div class="container">
    {{--<div class="pull-right col-md-offset-9"><a href="/users/logout">Log out</a></div>--}}

<div class="masthead col-md-offset-4">
    <h3 class="text-muted"></h3>
    <ul class="nav nav-justified ">
        <!-- <li><a href="index.php">Checkin Times</a></li> -->
        <li class="active"><a href="/dashboard">Dashboard</a></li>
        <li><a href="/dashboard">Payment</a></li>
        <li><a href="/dashboard">Settlement</a></li>
        <li><a href="/dashboard">SMS Campaigns</a></li>
        <li><a href="dashboard">Customers</a></li>
        <li><a href="dashboard">USSD Testing</a></li>
    </ul>
</div>
</div>