@extends('users.layouts.admin')

@section('content')
<div class="container">
		<div class="row">

					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="new-candidate">
							<button type="button" class="btn btn-success" data-toggle="modal" data-target="#candidateModal" data-whatever="@mdo">
								<i class="fa fa-plus"></i>Add Company
							</button>
						</div>
					</div>

					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<!-- BEGIN BORDERED TABLE PORTLET-->
						<div class="portlet box users-table">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-user"></i>Companies
								</div>
							</div>
							<div class="portlet-body">
								<table class="table table-bordered table-hover">
								<thead>

								<tr>
									<th>
										 #
									</th>
									<th>
										 NAME of Company
									</th>
									<th>Users</th>

									<th>
										 ACTION
									</th>
								</tr>
								</thead>
								<tbody>
								{{--@if($candidates)--}}
                                	{{--@foreach($candidates as $candidate)--}}
								{{--<tr>--}}
									{{--<td>--}}
										 {{--1--}}
									{{--</td>--}}
									{{--<td>--}}
										 {{--{{$candidate->names}}--}}
									{{--</td>--}}
									{{--<td class="hidden-480">--}}
										 {{--<a href="" class="btn btn-success" title="Edit">--}}
											{{--<i class="fa fa-pencil"></i>--}}
										{{--</a>--}}
									{{--</td>--}}

								{{--</tr>--}}
								{{--@endforeach--}}
								{{--@else--}}
								{{--<tr>No Candidates added</tr>--}}
								{{--@endif--}}
								<tr>
								<td></td>
								<td>KCB</td>
								<td>300</td>
								   <td class="hidden-480">
                                                                                                  <a href="#" class="btn btn-success" title="Approve">
                                                                                                      <i class="fa fa-check"></i>
                                                                                                   </a>
                                                                                                   <a href="#" class="btn btn-success" title="Delete">
                                                                                                       <i class="fa fa-ban"></i>
                                                                                                    </a>
                                                                                                </td>
								</tr>
								<tr>
                                <td></td>
                                <td>Equity</td>
                                 <td>500</td>
                                    <td class="hidden-480">
                                                                                                   <a href="#" class="btn btn-success" title="Approve">
                                                                                                       <i class="fa fa-check"></i>
                                                                                                    </a>
                                                                                                    <a href="#" class="btn btn-success" title="Ban">
                                                                                                        <i class="fa fa-ban"></i>
                                                                                                     </a>
                                                                                                 </td>
                                </tr>
								</tbody>
								</table>
							</div>
						</div>
						<!-- END BORDERED TABLE PORTLET-->
					</div>

				<!-- Add Candidate Modal begins here -->
						<div class="modal fade" id="candidateModal" tabindex="-1" role="dialog" aria-labelledby="candidateModalLabel" aria-hidden="true">
						  <div class="modal-dialog">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						        <h4 class="modal-title" id="candidateModalLabel">Adding Candidate</h4>
						      </div>
						      <div class="modal-body">
						        <form>
						          <div class="form-group">
						            <label for="recipient-name" class="control-label">Name Of Company:</label>
						            <input type="text" class="form-control" id="recipient-name">
						          </div>

						        </form>
						      </div>
						      <div class="modal-footer">
						        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						        <button type="button" class="btn btn-success">Submit</button>
						      </div>
						    </div>
						  </div>
						</div>
				<!-- Add Candidate Modal ends here -->

				<!-- Edit Candidate Modal begins here -->
						<div class="modal fade" id="editCandidateModal" tabindex="-1" role="dialog" aria-labelledby="editCandidateModalLabel" aria-hidden="true">
						  <div class="modal-dialog">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						        <h4 class="modal-title" id="editCandidateModalLabel">Editing Candidate Details</h4>
						      </div>
						      <div class="modal-body">
						        <form>
						          <div class="form-group">
						            <label for="recipient-name" class="control-label">Name Of Candidate:</label>
						            <input type="text" class="form-control" id="recipient-name" value="JCI SEN. TUNDE ANIFOWOSE-KELANI">
						          </div>
						          <div class="form-group">
						            <label for="recipient-name" class="control-label">Office:</label>
						            <input type="text" class="form-control" id="recipient-name">
						          </div>
						          <div class="form-group">
						            <label for="recipient-name" class="control-label">Party:</label>
						            <input type="text" class="form-control" id="recipient-name">
						          </div>
						          <div class="form-group">
									  <label for="sel1">Gender:</label>
									  <select class="form-control" id="sel1">
									    <option>Select Gender</option>
									    <option>Male</option>
									    <option>Female</option>
									  </select>
									</div>
						          <div class="form-group">
						            <label for="recipient-name" class="control-label">Age:</label>
						            <input type="text" class="form-control" id="recipient-name">
						          </div>
						          <div class="form-group">
						            <label for="recipient-name" class="control-label">Qualification:</label>
						            <input type="text" class="form-control" id="recipient-name">
						          </div>
						        </form>
						      </div>
						      <div class="modal-footer">
						        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						        <button type="button" class="btn btn-primary">Submit</button>
						      </div>
						    </div>
						  </div>
						</div>
				<!-- Edit Candidate Modal ends here -->

				</div>
	</div>

@stop