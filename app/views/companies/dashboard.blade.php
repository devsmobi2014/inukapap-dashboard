@extends('users.layouts.admin')

@section('content')
<div class="container">
    <!-- BEGIN DASHBOARD STATS -->
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-light red-soft" href="#">
                <div class="visual">
                    <i class="fa fa-building"></i>
                </div>
                <div class="details">
                    <div class="number">
                        {{count($company->employees);}}
                    </div>
                    <div class="desc">
                        No. of Employees Registered
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-light blue-soft" href="#">
                <div class="visual">
                    <i class="fa fa-users"></i>
                </div>
                <div class="details">
                    <div class="number">
                       {{$company->company_loan_amount;}}
                    </div>
                    <div class="desc">
                        Total unpaid Loans
                    </div>
                </div>
            </a>
        </div>

        {{--<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">--}}
            {{--<a class="dashboard-stat dashboard-stat-light red-soft" href="#">--}}
                {{--<div class="visual">--}}
                    {{--<i class="fa fa-money"></i>--}}
                {{--</div>--}}
                {{--<div class="details">--}}
                    {{--<div class="number">--}}
                       {{--Ksh {{$company->company_loan_amount}}--}}
                    {{--</div>--}}
                    {{--<div class="desc">--}}
                       {{--Value of loans disbursed--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</a>--}}
        {{--</div>--}}
    </div>
    <!-- END DASHBOARD STATS -->
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="new-candidate">
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#candidateModal" data-whatever="@mdo">
                    <i class="fa fa-plus"></i>Add Employee
                </button>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <!-- BEGIN BORDERED TABLE PORTLET-->
            <div class="portlet box users-table">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-money   "></i> {{$company->first_name." ".$company->last_name;}} Loans Due
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="javascript:;" class="reload">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                               Name of Employee
                            </th>
                            <th>
                                ID No:
                            </th>

                            <th>
                                Email:
                            </th>
                            <th>
                               Loan Limit
                            </th>
                            <th>
                                Loan advanced
                            </th>
                            {{--<th>--}}
                                {{--Loan limit--}}
                            {{--</th>--}}
                            {{--<th class="hidden-480">--}}
                                {{--Organization--}}
                            {{--</th>--}}
                        </tr>
                        </thead>
                        <tbody>

                        @if($company->employees)
                            <?php $i=1;?>

                        @foreach($company->employees as $employee)
                        <tr>
                            <td>
                                {{$i}}
                            </td>
                            <td>
                                {{--<a href="{{URL::to('user')}}/{{$employee->id}}">--}}
                                {{$employee->first_name." ".$employee->last_name;}}
                                {{--</a>--}}
                            </td>
                            <td>
                                {{--<a href="{{URL::to('user')}}/{{$employee->id}}">--}}
                                {{$employee->national_id}}
                                {{--</a>--}}
                            </td>
                            <td>
                                {{--<a href="{{URL::to('user')}}/{{$employee->id}}">--}}
                                {{$employee->email}}
                                {{--</a>--}}
                            </td>
                            <td>
                                {{--<a href="{{URL::to('user')}}/{{$employee->id}}">--}}
                                {{$employee->loan_limit}}
                                {{--</a>--}}
                            </td>
                            <td>
                                {{$employee->balance*-1;}}
                            </td>
                            {{--<td>--}}
										{{--<span class="label label-success">--}}
										{{--Ksh {{$->company_loan_amount;}} </span>--}}
                            {{--</td>--}}
                            {{--<td>--}}
                                {{--{{$vote->sum_polling_stations}}--}}
                            {{--</td>--}}
                        {{--</tr>--}}
                           <?php $i++; ?>
                        @endforeach
                        @else
                        <tr> No Loans due yet</tr>
                        @endif

                        </tbody>
                    </table>
                </div>
            </div>

            <!-- Add Candidate Modal begins here -->
            <div class="modal fade" id="candidateModal" tabindex="-1" role="dialog" aria-labelledby="candidateModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="candidateModalLabel">Adding Employee</h4>
                        </div>
                            <form method="POST" action="{{{ URL::to('employee/store') }}}">
                                <input type="hidden" name="company_id" value="{{$company->id}}">
                        <div class="modal-body">
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Name Of Employee:</label>
                                    <input type="text" class="form-control" name="employee-name" id="employee-name">
                                </div>

                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">ID Number:</label>
                                    <input type="text" class="form-control" name="employee-id" id="employee-id">
                                </div>

                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Phone:</label>
                                    <input type="text" class="form-control" name="employee-phone" id="employee-phone">
                                </div>


                                <div class="form-group">
                                    <label for="recipient-email" class="control-label">Email:</label>
                                    <input type="text" class="form-control" name="employee-email" id="employee-email">
                                </div>

                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Monthly Salary in Ksh:</label>
                                    <input type="text" class="form-control" name="employee-salary" id="employee-salary">
                                </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <input name="submit" type="submit" class="btn btn-success" value="Submit" />

                            {{--<button type="submit" class="btn btn-success">Submit</button>--}}
                        </div>
                            </form>
                    </div>
                </div>
            </div>
            <!-- Add Candidate Modal ends here -->
            <!-- END BORDERED TABLE PORTLET-->
        </div>
    </div>
</div>
@stop
