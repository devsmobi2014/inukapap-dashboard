@extends('layouts.dashlayout')
    @section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Sacco <small>statistics & reports</small></h1>
                </div>
                <!-- END PAGE TITLE -->

            </div>
        </div>
        <!-- END PAGE HEAD -->
        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMB -->
                <ul class="page-breadcrumb breadcrumb hide">
                    <li>
                        <a href="#">Home</a><i class="fa fa-circle"></i>
                    </li>
                    <li class="active">
                        Dashboard
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMB -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row margin-top-10">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <a class="dashboard-stat dashboard-stat-light blue-soft" href="#">
                            <div class="visual">
                                <i class="fa fa-users"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    {{count($sacco_users)}}
                                </div>
                                <div class="desc">
                                    Number of members
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <a class="dashboard-stat dashboard-stat-light purple-soft" href="#">
                            <div class="visual">
                                <i class="fa fa-briefcase"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                   {{$no_of_loans}}
                                </div>
                                <div class="desc">
                                    Number of Loans
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <a class="dashboard-stat dashboard-stat-light green-soft" href="#">
                            <div class="visual">
                                <i class="fa fa-shopping-cart"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    Ksh {{$value_of_loans}}
                                </div>
                                <div class="desc">
                                    Loans Disbursed
                                </div>
                            </div>
                        </a>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-briefcase font-green-sharp"></i>
                                    <span class="caption-subject font-green-sharp bold uppercase">SACCO NAME:: {{$sacco->name}}</span>
                                </div>
                                <div class="caption pull-right">
                                    <i class="fa fa-plus font-blue-sharp"></i>
                                    <a href="{{url('/member/create/'.$sacco->id)}}"><span class="caption-subject font-blue-sharp bold uppercase">Add Member</span></a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-scrollable">
                                    <table class="table table-striped table-bordered table-advance table-hover">
                                        <thead>
                                        <tr>
                                            <th>
                                                <i class="fa fa-user"></i> Applicant
                                            </th>
                                            <th class="hidden-xs">
                                                <i class="fa fa-phone"></i> Phone Number
                                            </th>
                                            <th>
                                                <i class="fa fa-check-circle-o"></i> ID Number
                                            </th>
                                            <th>
                                                <i class="fa fa-money"></i> Account balance
                                            </th>
                                            {{--<th>--}}
                                                {{--<i class="fa fa-tasks"></i> Status--}}
                                            {{--</th>--}}
                                            {{--<th>--}}
                                                {{--<i class="fa fa-clock-o"></i> Expected pay date--}}
                                            {{--</th>--}}
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($sacco_users as $user)
                                        <tr>
                                            <td class="highlight">
                                                {{$user->first_name }} {{ $user->last_name }}
                                            </td>
                                            <td class="hidden-xs">
                                                {{$user->phone }}
                                            </td>
                                            <td>
                                                {{$user->national_id }}
                                            </td>
                                            <td>
                                                Ksh. {{$user->balance}}
                                            </td>
                                            {{--<td>--}}
										{{--<span class="label label-sm label-success">--}}
										{{--<i class="fa fa-check"></i> Paid </span>--}}
                                            {{--</td>--}}
                                            {{--<td>--}}
                                                {{--22/10/2015--}}
                                            {{--</td>--}}
                                        </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
    @stop
