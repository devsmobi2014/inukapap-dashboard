@extends('layouts.dashlayout')
    @section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Add members to {{$sacco->name}}</h1>
                </div>
                <!-- END PAGE TITLE -->

            </div>
        </div>
    <div class="page-content">
        <div class="container">
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb breadcrumb hide">
                <li>
                    <a href="#">Home</a><i class="fa fa-circle"></i>
                </li>
                <li class="active">
                    Dashboard
                </li>
            </ul>
            <!-- END PAGE BREADCRUMB -->
            <!-- BEGIN PAGE CONTENT INNER -->

            <div class="row reg-new">
                <div class="col-md-12">
                    <?php echo $errors->first('name'); ?>
                        <p class="bg-warning">
                    {{ Session::get('message') }}</p>
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-briefcase"></i>Register member
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse">
                                </a>
                                <a href="#portlet-config" data-toggle="modal" class="config">
                                </a>
                                <a href="javascript:;" class="reload">
                                </a>
                                <a href="javascript:;" class="remove">
                                </a>
                            </div>
                        </div>

                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form method="post" action="{{url('member/store')}}" class="form-horizontal">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">First Name</label>
                                        <div class="col-md-4">
                                            <input type="text" value="{{Input::old('first_name')}}" name="first_name" class="form-control input-circle" placeholder="e.g. Leonard">

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Last Name</label>
                                        <div class="col-md-4">
                                            <input type="text" value="{{Input::old('last_name')}}" name="last_name" class="form-control input-circle" placeholder="e.g. Waweru">

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Phone No</label>
                                        <div class="col-md-4">
                                            <input type="text" value="{{Input::old('phone')}}" name="phone" class="form-control input-circle" placeholder="e.g. 0722123456">

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">National ID Number</label>
                                        <div class="col-md-4">
                                            <input type="text" value="{{Input::old('national_id')}}" name="national_id" class="form-control input-circle" placeholder="e.g. 1234567">

                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" value="{{$sacco->id}}" name="org_id">
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" class="btn btn-circle green">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->
                </div>
            </div>
            <!-- END PAGE CONTENT INNER -->
        </div>
    </div>
  @stop