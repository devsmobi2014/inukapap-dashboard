@extends('users.layouts.admin')

@section('content')
<div class="container">
	<div class="row">

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="add-polling-station">
			<button  type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#pollingStationModal" data-whatever="@mdo">
				<i class="fa fa-plus"></i>Add to Invite List
				</button>
			</div>
		</div>
	</div>
		<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<!-- BEGIN BORDERED TABLE PORTLET-->
			<div class="portlet box users-table">
				<div class="portlet-title">
					<div class="caption">
						<!--<i class="fa fa-user"></i>-->
						Invited Users
					</div>
					<div class="tools">
						<a href="javascript:;" class="collapse"> </a>
						<a href="#portlet-config" data-toggle="modal" class="config"> </a>
						<a href="javascript:;" class="reload"> </a>
						<a href="javascript:;" class="remove"> </a>
					</div>
				</div>
				<div class="portlet-body table-responsive">
					<table class="table table-bordered table-hover">
                    								<thead>
                    								<tr>

                    									<th>
                    										First Name
                    									</th>
                    									<th>
                    										Last Name
                    									</th>
                    									<th class="hidden-480">
                    										 Phone
                    									</th>
                    									<th>
                    										 Status
                    									</th>
                    									<th>
                                                        	 Organisation
                                                        </th>
                    								</tr>
                    								</thead>
                    								<tbody>
                    								{{--@if($users)--}}

                    								{{--@foreach($users as $user)--}}
                    								{{--<tr>--}}
                    									{{--<td>--}}
                    										{{--{{$i=1}}--}}
                    									{{--</td>--}}
                    									{{--<td>--}}
                    										{{--{{$user->phone}}--}}
                    									{{--</td>--}}
                    									{{--<td>--}}
                    										 {{--{{$user->polling_station_name}}--}}
                    									{{--</td>--}}
                    									{{--<td>--}}
                    									   {{--@if($user->status ==1)--}}
                    										{{--<span class="label label-success">--}}
                    										{{--Fully Reported </span>--}}
                    										{{--@elseif($user->status==2)--}}
                    										{{--<span class="label label-alert">--}}
                                                              {{--Half Reported </span>--}}
                    										{{--@else--}}
                    										  {{--<span class="label label-success">--}}
                                                                {{--Not yet Reported </span>--}}
                    										{{--@endif--}}
                    									{{--</td>--}}
                    									{{--<td class="hidden-480">--}}
                    										 {{--<a href="{{URL::to('user/edit')}}/{{$user->id}}" class="btn btn-success" title="Edit">--}}
                    											{{--<i class="fa fa-pencil"></i>--}}
                    										{{--</a>--}}
                    									{{--</td>--}}

                    								{{--</tr>--}}
                    								{{--<tr>--}}
                    								 {{--{{$i++}}--}}
                    								{{--@endforeach--}}
                    								{{--@else--}}
                    								  {{--<tr>Users not added yet</tr>--}}
                    								{{--@endif--}}
                    								<tr>
                    								<td>John </td>
                    								<td>Doe</td>
                    								<td>071111111</td>
                    								<td>Pending</td>
                    								<td>KCB</td>
                    								</tr>
                    								<tr>
                                                    <td>John </td>
                                                    <td>Doe</td>
                                                    <td>071111111</td>
                                                    <td>Sent</td>
                                                    <td>KCB</td>
                                                    </tr>
                    								</tbody>

					</table>
				</div>
			</div>
			<!-- Add polling station Modal begins here -->
						<div class="modal fade" id="pollingStationModal" tabindex="-1" role="dialog" aria-labelledby="userModalLabel" aria-hidden="true">
						  <div class="modal-dialog">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						        <h4 class="modal-title" id="userModalLabel">Adding Polling Station</h4>
						      </div>
						      <div class="modal-body">
						        <form>
						          <div class="form-group">
						            <label for="recipient-name" class="control-label">First Name:</label>
						            <input type="text" class="form-control" id="recipient-name">
						          </div>
						          <div class="form-group">
						            <label for="recipient-name" class="control-label">Last Name</label>
						            <input type="text" class="form-control" id="recipient-name">
						          </div>
						          <div class="form-group">
						            <label for="recipient-name" class="control-label">Phone</label>
						            <input type="text" class="form-control" id="recipient-name">
						          </div>

						        </form>
						      </div>
						      <div class="modal-footer">
						        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						        <button type="button" class="btn btn-success">Submit</button>
						      </div>
						    </div>
						  </div>
						</div>
						<!-- End Modal -->
						<!-- Send polling station result Notice Modal begins here -->
						<div class="modal fade" id="sendResult" tabindex="-1" role="dialog" aria-labelledby="sendResult" aria-hidden="true">
						  <div class="modal-dialog">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						        <h4 class="modal-title" id="sendResult">Send Result</h4>
						      </div>
						      <div class="modal-body">
						        <p>The selected polling station result will be sent</p>
						      </div>
						      <div class="modal-footer">
						        <a  class="btn btn-default" data-dismiss="modal">Cancel</a>
						        <a href="polling-stations.php" class="btn btn-success"><i class="fa fa-check"></i> Delete</a>
						      </div>
						    </div>
						  </div>
						</div>
				<!-- Delete issue Notice Modal ends here -->

			<!-- END BORDERED TABLE PORTLET-->
		</div>



				</div>
	</div>
	@stop