@extends('users.layouts.admin')

@section('content')

<div class="container">
    {{--
    <div class="pull-right col-md-offset-9"><a href="/users/logout">Log out</a></div>
    --}}
    <table class="table well">

        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-6">
                <div id="emulator-main">
                    <div id="emulator">

                    </div>
                    <div id="reply">
                        <form id="simlator">

                            <div class="input-group">
                                <input type="text" name="message" class="form-control" placeholder="Enter Message...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary" id="endMessage" type="button">
                                            Send
                                        </button>
                                    </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </table>
</div>
@stop
