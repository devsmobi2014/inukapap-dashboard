@extends('users.layouts.user')

@section('content')

<div class="container">
    <div class="row">
        <br>
        <div class="col-md-4 col-md-offset-4">
            <hr />
            <div class="well">
                <form method="post" action="{{ URL::to('users/store') }}" >

                    <h1>Sign up </h1>

                    <div class="label" style="color:black;">Username:</div>
                    <input name="username" type="text" class="field form-control" id="username"/>
                    <br />

                    <div class="label" style="color:black;">Phone:</div>
                    <input name="phone" type="text" class="field form-control" id="phone"/>
                    <br />

                    <div class="email" style="color:black;">Email:</div>
                    <input name="email" type="text" class="field form-control" id="email"/>
                    <br />

                    <div class="label" style="color:black;">Password:</div>
                    <input name="password" type="password" class="field form-control" id="password"/>
                    <br />

                    <div class="label" style="color:black;">Confirm password:</div>
                    <input name="password_confirmation" type="password" class="field form-control" id="password_confirmation"/>
                    <br />

                    <!-- <div class="right">
                    <label>Remember Me For 30 Days
                    <input name="remember" type="checkbox" value="true" />
                    </label>
                    </div> -->

                    <input name="page" type="hidden" />

                    <input name="submit" type="submit" class="btn btn-info btn-block" value="Sign up " />

                    <br />
                    <br />
                    <div class="center">
                    Have an account? <a href="/users/login">Log in here</a>
                    </div>

                </form>


            </div>
            @if ( Session::get('error') )
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif

            @if ( Session::get('notice') )
                <div class="alert">
                    {{ Session::get('notice') }}
                </div>
            @endif
            {{--<div class="forget-password">--}}
                {{--<p>--}}
                    {{--Forget password? no worries, click <a  href="{{URL::to('users/forgot_password')}}" id="forget-password">{{ Lang::get('confide::confide.login.forgot_password') }}</a>--}}
                    {{--to reset your password.--}}
                {{--</p>--}}
            {{--</div>--}}
        </div>
    </div>
</div>
@stop
