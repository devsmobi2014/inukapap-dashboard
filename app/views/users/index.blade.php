@extends('users.layouts.admin')

@section('content')
<div class="container">
		<div class="row">

					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="new-user">
							<button type="button" class="btn btn-success" data-toggle="modal" data-target="#userModal" data-whatever="@mdo">
								<i class="fa fa-plus"></i>Add Admin
							</button>
						</div>

						<!-- Modal begins here -->
						<div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="userModalLabel" aria-hidden="true">
						  <div class="modal-dialog">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						        <h4 class="modal-title" id="userModalLabel">Adding New User</h4>
						      </div>
						       <form action="{{URL::to('users/storeuser')}}" method="post">
						      <div class="modal-body">

						          <div class="form-group">
						            <label for="recipient-name" class="control-label">First Name:</label>
						            <input type="text" required="" name="first_name" class="form-control" id="recipient-name">
						          </div>
						          <div class="form-group">
                                  		<label for="recipient-name" class="control-label">Last name:</label>
                                  		 <input type="text" required="" name="last_name" class="form-control" id="recipient-name">
                                  	</div>
						          <div class="form-group">
						            <label for="recipient-name" class="control-label">Password:</label>
						            <input type="password" required="" name="password" class="form-control" id="recipient-name">
						          </div>
						          <div class="form-group">
                                  	 <label for="recipient-name" class="control-label">Repeat Password:</label>
                                  	 <input type="password" required="" name="repeatpassword" class="form-control" id="recipient-name">
                                  </div>
						          {{--<button type="button" type="submit" class="btn btn-success">Submit</button>--}}

						      </div>
						      <div class="modal-footer">
						        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						        <button type="submit" name="submit" value="submit" class="btn btn-success">Submit</button>
						      </div>
						       </form>
						    </div>
						  </div>
						</div>

					</div>

					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<!-- BEGIN BORDERED TABLE PORTLET-->
						<div class="portlet box users-table">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-user"></i>Users/Loanees
								</div>
							</div>
							<div class="portlet-body">
								<table class="table table-bordered table-hover">
								<thead>
								<tr>

									<th>
										First Name
									</th>
									<th>
										Last Name
									</th>
									<th class="hidden-480">
										 Phone
									</th>
									<th>
										 Loan
									</th>
									<th>
                                    	 Organisation
                                    </th>
                                    <th>
                                        Status
                                     </th>
                                     <th>
                                       Action
                                       </th>
								</tr>
								</thead>
								<tbody>
								{{--@if($users)--}}

								{{--@foreach($users as $user)--}}
								{{--<tr>--}}
									{{--<td>--}}
										{{--{{$i=1}}--}}
									{{--</td>--}}
									{{--<td>--}}
										{{--{{$user->phone}}--}}
									{{--</td>--}}
									{{--<td>--}}
										 {{--{{$user->polling_station_name}}--}}
									{{--</td>--}}
									{{--<td>--}}
									   {{--@if($user->status ==1)--}}
										{{--<span class="label label-success">--}}
										{{--Fully Reported </span>--}}
										{{--@elseif($user->status==2)--}}
										{{--<span class="label label-alert">--}}
                                          {{--Half Reported </span>--}}
										{{--@else--}}
										  {{--<span class="label label-success">--}}
                                            {{--Not yet Reported </span>--}}
										{{--@endif--}}
									{{--</td>--}}
									{{--<td class="hidden-480">--}}
										 {{--<a href="{{URL::to('user/edit')}}/{{$user->id}}" class="btn btn-success" title="Edit">--}}
											{{--<i class="fa fa-pencil"></i>--}}
										{{--</a>--}}
									{{--</td>--}}

								{{--</tr>--}}
								{{--<tr>--}}
								 {{--{{$i++}}--}}
								{{--@endforeach--}}
								{{--@else--}}
								  {{--<tr>Users not added yet</tr>--}}
								{{--@endif--}}
								<tr>
								<td>John </td>
								<td>Doe</td>
								<td>071111111</td>
								<td>20000</td>
								<td>KCB</td>
								<td>requested</td>
								<td class="hidden-480">
                                  <a href="#" class="btn btn-success" title="Approve">
                                      <i class="fa fa-check"></i>
                                   </a>
                                   <a href="#" class="btn btn-success" title="Ban">
                                       <i class="fa fa-ban"></i>
                                    </a>
                                </td>
								</tr>
								<tr>
                                <td>Jane </td>
                                <td>Doe</td>
                                <td>073333333</td>
                                <td>40000</td>
                                <td>KCB</td>
                                <td>loaned</td>
                                <td class="hidden-480">
                                                                  <a href="#" class="btn btn-success" title="Approve">
                                                                      <i class="fa fa-check"></i>
                                                                   </a>
                                                                   <a href="#" class="btn btn-success" title="Ban">
                                                                       <i class="fa fa-ban"></i>
                                                                    </a>
                                                                </td>
                                </tr>
								</tbody>
								</table>
							</div>
						</div>
						<!-- END BORDERED TABLE PORTLET-->
					</div>
				</div>
	</div>
	@stop