@extends('users.layouts.admin')

@section('content')
<div class="container">
    <!-- BEGIN DASHBOARD STATS -->
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <a class="dashboard-stat dashboard-stat-light red-soft" href="#">
                <div class="visual">
                    <i class="fa fa-building"></i>
                </div>
                <div class="details">
                    <div class="number">
                        {{count($companies);}}
                    </div>
                    <div class="desc">
                        Companies
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <a class="dashboard-stat dashboard-stat-light blue-soft" href="#">
                <div class="visual">
                    <i class="fa fa-users"></i>
                </div>
                <div class="details">
                    <div class="number">
                       {{$users;}}
                    </div>
                    <div class="desc">
                        Users
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <a class="dashboard-stat dashboard-stat-light red-soft" href="#">
                <div class="visual">
                    <i class="fa fa-money"></i>
                </div>
                <div class="details">
                    <div class="number">
                       Ksh {{$loan_amount}}
                    </div>
                    <div class="desc">
                       Value of loans disbursed
                    </div>
                </div>
            </a>
        </div>
    </div>
    <!-- END DASHBOARD STATS -->
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="new-candidate">
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#companymodal" data-whatever="@mdo">
                    <i class="fa fa-plus"></i>Add Company
                </button>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <!-- BEGIN BORDERED TABLE PORTLET-->
            <div class="portlet box users-table">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-money "></i> Companies
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="javascript:;" class="reload">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                               Company Name
                            </th>
                            <th>
                               No. of Users
                            </th>
                            <th>
                                Total loan due
                            </th>
                            {{--<th class="hidden-480">--}}
                                {{--Organization--}}
                            {{--</th>--}}
                        </tr>
                        </thead>
                        <tbody>

                        @if($companies)


                        @foreach($companies as $company)
                        <tr>
                            <td>
                                {{$i}}
                            </td>
                            <td>
                                <a href="{{URL::to('company')}}/{{$company->id}}">
                                {{$company->first_name." ".$company->last_name;}}
                                </a>
                            </td>
                            <td>
                                {{count($company->employees)}}
                            </td>
                            <td>
										{{--<span class="label label-success">--}}
										Ksh {{$company->company_loan_amount;}} </span>
                            </td>

                            {{--{{$i++}}--}}
                            <?php $i++; ?>
                        @endforeach
                        @else
                        <tr> No Loans due yet</tr>
                        @endif

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END BORDERED TABLE PORTLET-->
            <!-- Add Candidate Modal begins here -->
            <div class="modal fade" id="companymodal" tabindex="-1" role="dialog" aria-labelledby="candidateModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="candidateModalLabel">Adding Company</h4>

                        </div>
                        <form method="POST" action="{{{ URL::to('company') }}}">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Name Of Company:</label>
                                    <input type="text" required="required" class="form-control" name="name" id="name">
                                </div>

                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Phone:</label>
                                    <input type="text"  required="required"class="form-control" name="phone" id="phone">
                                </div>


                                <div class="form-group">
                                    <label for="recipient-email" class="control-label">Contact Email:</label>
                                    <input type="text" required="required" class="form-control" name="email" id="email">
                                </div>


                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <input name="submit" type="submit" class="btn btn-success" value="Submit" />

                                {{--<button type="submit" class="btn btn-success">Submit</button>--}}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
