<nav class="navbar navbar-default navbar-fixed-top">

	<div class="container">
		<div class="navbar-header">
			<div id="navbar">
				<ul id="gn-menu" class="gn-menu-main nav">
					<li class="gn-trigger pull-left">
						<a class="gn-icon gn-icon-menu"><span>Menu</span></a>
						<nav class="gn-menu-wrapper">
							<div class="gn-scroller">
								<ul class="gn-menu">
									<li class="gn-search-item">
										<input placeholder="Search" type="search" class="gn-search">
										<a href="#" class="gn-icon gn-icon-search"><span>Search</span></a>
									</li>
									<li>
										<a href="#" class="gn-icon gn-icon-download">Edit USSD Menu </a>
										<!--<ul class="gn-submenu">
										<li><a class="gn-icon gn-icon-illustrator">Latest Changes</a></li>
										<li><a class="gn-icon gn-icon-photoshop">Sample USSD menu</a></li>
										</ul>-->
									</li>
									<li>
										<a href="#" class="gn-icon gn-icon-cog">Settings</a>
									</li>
									<li>
										<a href="#" class="gn-icon gn-icon-help">Help</a>
									</li>
								</ul>
							</div><!-- /gn-scroller -->
						</nav>
					</li>
					<li>
						<a href="/dashboard">Dashboard</a>

					</li>
					{{--<li>--}}
						{{--<a class="" href="/users">Users</a>--}}
					{{--</li>--}}
					{{--<li>--}}
						{{--<a class="" href="/invitations">Invitations</a>--}}
					{{--</li>--}}
					{{--<li>--}}
						{{--<a class="" href="/companies">Company/clients</a>--}}
					{{--</li>--}}
					{{--<li>--}}
						{{--<a class="" href="/issues">Issues</a>--}}
					{{--</li>--}}
                    {{--<li>--}}
                        {{--<a class="" href="/emulator">USSD Emulator</a>--}}
                    {{--</li>--}}

					<li>
						<a class="" href="/users/logout">logout</a>
					</li>
				</ul>

			</div>
		</div>
	</div>
</nav>
