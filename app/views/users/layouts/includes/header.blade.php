<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Inukapap</title>
		<meta name="description" content="Inukapap" />
		<meta name="keywords" content="Inukapap" />
		<meta name="author" content="{mobidev}" />
		<link rel="shortcut icon" href="../favicon.ico">
		<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css" />
		<link rel="stylesheet" type="text/css" href="/css/custom.css" />
        <link href="/dist/css/custom.css" rel="stylesheet">
		<script src="/js/modernizr.custom.js"></script>
	</head>

