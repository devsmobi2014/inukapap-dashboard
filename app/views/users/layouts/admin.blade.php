@include('users/layouts/includes/header')
@include('users/layouts/includes/menu')
<body>
<div class="top-clearfix"></div>
@yield('content')
@include('users/layouts/includes/footer')