<?php

class UssdTest extends TestCase {

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $crawler = $this->client->request('GET', '/');

        $this->assertTrue($this->client->getResponse()->isOk());
    }
    public function testMainMenu(){
        $response = $this->call('GET', 'ussd/menu');
        $this->assertEquals('Welcome to Results Transmission\n 1.Enter pin', $response->getContent());
    }


}
