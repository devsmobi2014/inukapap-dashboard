<?php

class UssdLogs extends \Eloquent {

	protected $fillable = ['phone','text'];
	protected $table = 'ussd_logs';

}