<?php


class UssdUser extends Eloquent
{
    protected $fillable = ['phone','username','session','pin','national_id','progress','menu_item_id','step'];
    protected $table = 'users';

    protected $dates = ['deleted_at'];




}