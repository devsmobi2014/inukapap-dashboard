<?php

class ElectionReport extends \Eloquent
{
    protected $fillable = ['user_id', 'polling_station_id', 'result_type_id', 'results', 'status'];
    protected $table = "election_report";


    public static function getCandidateVotes()
    {
        $votes = DB::table('election_report')
            ->join('result_type', 'result_type.id', '=', 'election_report.result_type_id')
            ->join('candidates', 'candidates.id', '=', 'result_type.candidate_id')
            ->select(DB::raw('candidates.names, candidates.party, sum(election_report.results) as result, count(DISTINCT election_report.polling_station_id) as sum_polling_stations'))
            ->groupBy('candidates.id')
            ->get();

        return $votes;
    }
    public  static function getResults($user_id)
    {
        $votes = DB::table('election_report')
            ->join('result_type', 'result_type.id', '=', 'election_report.result_type_id')
            ->join('polling_stations','polling_stations.id','=','election_report.polling_station_id')
            ->select(DB::raw('election_report.id,result_type.names,result_type.id as result_type_id, election_report.polling_station_id,election_report.results as result,polling_stations.unique_ward_code '))
            ->where('election_report.user_id','=',$user_id)
            ->groupBy('result_type.id')
            ->get();

        return $votes;
    }
    public static function getUniqueWard($user_id){
        $polling_station=DB::table('election_report')
              ->join('polling_stations','polling_stations.id','=','election_report.polling_station_id')
              ->select(DB::raw('polling_stations.unique_ward_code '))
              ->where('election_report.user_id','=',$user_id)
              ->first();
        if($polling_station){
            return $polling_station->unique_ward_code;
        }else{
            return false;
        }

    }
}