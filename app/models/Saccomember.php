<?php

class Saccomember extends \Eloquent {

	protected $fillable = ['first_name','username','last_name','phone','national_id','org_id'];
	protected $table = 'users';
}