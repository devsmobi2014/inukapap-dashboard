<?php

class UssdResponses extends \Eloquent {
	protected $fillable = ['user_id','menu_id','response','step','response'];
	protected $table = 'user_ussd_response';

}