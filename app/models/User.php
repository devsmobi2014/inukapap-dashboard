<?php

use Zizaco\Confide\ConfideUser;
use Zizaco\Confide\ConfideUserInterface;

class User extends Eloquent implements ConfideUserInterface
{
    use ConfideUser;
    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];
    //protected $fillable = ['first_name','last_name','org_id','national_id','loan_limit','email','monthly_salary'];

//    public  static function getUsers(){
//        $users=DB::table('users')
//            ->join('polling_stations', 'users.code', '=', 'polling_stations.id')
//            ->select('users.id', 'users.phone', 'polling_stations.status','polling_stations.polling_station_name')
//            ->where('users.role','=',2)
//            ->paginate(20);
//        return $users;
//    }

    public  function employees(){
        return $this->hasMany('Employee', 'org_id', 'id');
    }
}