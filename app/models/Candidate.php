<?php

class Candidate extends Eloquent
{
    public static function getCandidatesSummary()
    {
        $polling_stations = DB::table('polling_stations')
            ->join('election_report', 'polling_stations.id', '=', 'election_report.polling_station_id')
            ->select(DB::raw('sum(election_report.results) as result,polling_stations.state_name, polling_stations.state_code,polling_stations.status,polling_stations.polling_station_name,polling_stations.lga_name,polling_stations.lga_code,polling_stations.ward_name,polling_stations.ward_code,polling_stations.unique_ward_code,polling_stations.id,polling_stations.polling_station_code,polling_stations.status'))
            ->groupBy('polling_stations.id')
            ->get();

        return $polling_stations;
    }



}

