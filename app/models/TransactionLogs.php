<?php

class TransactionLogs extends \Eloquent {

	protected $fillable = ['user_id','type_id','amount'];
	protected $table = 'transaction_logs';

}