<?php

use Illuminate\Database\Migrations\Migration;

class ConfideSetupUsersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        // Creates the users table
        Schema::create('users', function ($table) {
            $table->increments('id');
            $table->string('username')->unique();
            $table->string('first_name', 45);
            $table->string('last_name', 45);
            $table->string('phone', 45);
            $table->string('code', 45);
            $table->string('pin', 45);
            $table->string('email')->unique()->nullable();
            $table->integer('session');
            $table->integer('role_id');
            $table->string('password');
            $table->string('monthly_salary');
            $table->string('account_balance');
            $table->string('profile_pic');
            $table->string('merchant_code');
            $table->string('confirmation_code');
            $table->string('remember_token')->nullable();
            $table->boolean('confirmed')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });

        // Creates password reminders table
        Schema::create('password_reminders', function ($table) {
            $table->string('email');
            $table->string('token');
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('password_reminders');
        Schema::drop('users');
    }
}
