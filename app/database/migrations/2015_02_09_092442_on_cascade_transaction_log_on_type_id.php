<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OnCascadeTransactionLogOnTypeId extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('transaction_logs', function(Blueprint $table)
		{
			//
			$table->foreign('type_id')->references('id')->on('transaction_type')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('transaction_logs', function(Blueprint $table)
		{
			//
		});
	}

}
