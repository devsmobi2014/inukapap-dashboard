<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class TransactionTypeTableSeeder extends Seeder {

	public function run()
	{

		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		DB::table('transaction_type')->truncate();
		DB::table('transaction_type')->delete();

		$transaction_type = array(
			array(
				'name' => "Loan Request",
			),
			array(
				'name' => 'Loan Repayment',
			),


		);

		DB::table('transaction_type')->insert($transaction_type);



	}

}