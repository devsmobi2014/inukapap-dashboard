<?php

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // Add calls to Seeders here
        //$this->call('UsersTableSeeder');
        $this->call('TransactionTypeTableSeeder');

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

}
