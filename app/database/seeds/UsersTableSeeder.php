<?php


class UsersTableSeeder extends Seeder
{

    public function run()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('users')->truncate();
        DB::table('users')->delete();


        $users = array(
            array(
                'username' => 'admin',
                'email' => 'admin@devs.mobi',
                'password' => Hash::make('inukapap2015'),
                'first_name' => 'Inukapap',
                'last_name' => 'Admin',
                'confirmed' => 1,
                'phone' => null,
                'role' =>1,
                'org_id'=>0,
                'confirmation_code' => md5(microtime() . Config::get('app.key')),
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'username' => 'MobiDev',
                'email' => 'hr@devs.mobi',
                'password' => Hash::make('mobidev2015'),
                'first_name' => 'MobiDev',
                'last_name' => 'Kenya',
                'confirmed' => 1,
                'phone' => null,
                'role' =>2,
                'org_id'=>0,
                'confirmation_code' => md5(microtime() . Config::get('app.key')),
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'username' => 'user 1',
                'email' => 'employee@devs.mobi',
                'password' => Hash::make('inukapap2015'),
                'first_name' => 'Employee',
                'last_name' => 'One',
                'confirmed' => 1,
                'phone'=> '0728355429',
                'role' =>3,
                'org_id'=>2,
                'confirmation_code' => md5(microtime() . Config::get('app.key')),
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            )
        );

        DB::table('users')->insert($users);

    }

}