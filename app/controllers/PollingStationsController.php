<?php

class PollingStationsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /pollingstations
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$polling_stations=PollingStation::getPollingStations();
        $stations="";



		return View::make('polling/index',compact('polling_stations','stations'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /pollingstations/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /pollingstations
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /pollingstations/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /pollingstations/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /pollingstations/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /pollingstations/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	public function result(){

		$polling_result=PollingStation::getPollingStationResult()->toArray();

		$unique_ward_codes=PollingStation::groupBy('unique_ward_code')->paginate(50);
		$result_array=[];
        foreach($unique_ward_codes as $code){
			foreach($polling_result['data'] as  $result){

                 if($result->unique_ward_code==$code->unique_ward_code){
					 array_push($result_array,$result);
				 }
			}
		}
		print_r($result_array);
		exit;

		return View::make('polling.result',compact('polling_result'));
	}

}