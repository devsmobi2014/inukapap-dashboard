<?php

class PollinStationController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /pollinstation.php
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$polling_stations=PollingStation::getPollingStations();

		return View::make('candidates.index');
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /pollinstation.php/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /pollinstation.php
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$validator = Validator::make($data = Input::all(), PollinStationController::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		PollinStationController::create($data);
	}

	/**
	 * Display the specified resource.
	 * GET /pollinstation.php/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /pollinstation.php/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /pollinstation.php/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /pollinstation.php/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}