<?php



/**
 * UsersController Class
 *
 * Implements actions regarding user management
 */
class MenuController extends Controller
{

    /**
     * Displays the form for account creation
     *
     * @return  Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('menu.create');
    }

    public function store()
    {

    }

    public function getMenu()
    {

    }


}
