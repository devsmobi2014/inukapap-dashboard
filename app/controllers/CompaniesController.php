<?php


class CompaniesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /companies
	 *
	 * @return Response
	 */

	public function index()
	{
		//
		return View::make('companies.index',compact('candidates'));
	}
	public function company($company_id){
		$company = User::find($company_id);
//		print_r($company);
//		exit;
		$users = 0;
		$company_loan_amount = 0;
		$employees = User::whereOrgId($company->id)->get();
		$company->employees = $employees;
		$users = $users + count($employees);
		foreach ($employees as $employee){
			if($employee->balance < 0){
				$company_loan_amount = $company_loan_amount - $employee->balance;
			}
		}
		$company->company_loan_amount = $company_loan_amount;


		return View::make('companies.dashboard',compact('users','company'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /companies/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
        return View::make('companies.create');

	}

	//create a new member
//	public function membercreate($id)
//	{
//		$sacco = Sacco::find($id);
//		return View::make('companies.create',compact('sacco'));
//
//	}
	/**
	 * Store a newly created resource in storage.
	 * POST /companies
	 *
	 * @return Response
	 */
	public function store()
	{
		//]]
        $input = Input::all();
        if($input['name']==""||$input['phone']==""||$input['email']==""){
            return Redirect::to('dashboard');
        }

        $user = new UssdUser();
        $user->first_name =$input['name'];
        $user->phone=$input['phone'];
		$user->username = $input['name'];
        $user->email = $input['email'];
        $user->role=2;
        if($user->save()){
            return Redirect::to('dashboard');
        }else{
            return Redirect::to('dashboard');
        }



	}
    public function storeSacco(){
        $input = Input::all();

        $validator=Validator::make(
            array('name' => $input['name']),
            array('name' => array('required','min:2'))
        );
        if($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $res=Sacco::create($input);

        return Redirect::to('sacco/view/'.$res->id)->with('sacco', $res);

    }

	public function storeemployee()
	{

		require_once('SmsController.php');

		$input = Input::all();
		$user = new UssdUser();
		$name = explode(" ", $input['employee-name'], 2);
		$user->username = $input['employee-phone'];
		$user->first_name = $name[0];

		if (!empty($name[1])) {
		$user->last_name = $name[1];
		}
		$user->phone = "254".trim(substr($input['employee-phone'], -9));
		$user->org_id = $input['company_id'];
		$user->national_id = trim($input['employee-id']);
		//$user->loan_limit = $input['employee-salary']/3;
		$user->loan_limit = 5000;
		//$input['employee-salary']/3;
		$user->email = $input['employee-email'];
		$user->monthly_salary = $input['employee-salary'];
        $user->confirmed=0;

		$company = User::find($input['company_id']);

		if ($user->save()) {
			$SmsController    = new SmsController();

			$message = "Your company ".$company->first_name." ".$company->last_name." has signed you to Inukapap Salary Advance loan service. Dial *384*2014# to complete the signup process";
			$result = $SmsController->sendSms($user->phone,$message);
			return Redirect::to('/company/'.$input['company_id']);
		}else{

		}

	}

	/**
	 * Display the specified resource.
	 * GET /companies/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$sacco = Sacco::find($id);
		$sacco_users = UssdUser::whereOrgId($id)->get();
//		print_r($sacco_users);
//		exit;
		$no_of_loans = 0;
		$value_of_loans = 0;
		foreach($sacco_users as $su){

			if($su->balance < 0){
				$no_of_loans++;
				$value_of_loans = $value_of_loans - $su->balance;
			}
		}
        return View::make('companies.view',compact('sacco','sacco_users','no_of_loans','value_of_loans'));

	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /companies/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /companies/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /companies/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}