<?php
use Illuminate\Http\Response;
Use Illuminate\Support\Facades\Input;
///use

class UssdController extends \BaseController
{

    /**
     * Display a listing of the resource.
     * GET /ussd
     *
     * @return Response
     */

    public function index()
    {
        //Get user request
        $params = self::getRequestDetails();

        //check if this is the first message from the user

        if (!empty($params['message'])) {
            $result = explode("*", $params['message']);
            if (empty($result)) {
                //message has no *

            } else {
                end($result);
                // move the internal pointer to the end of the array
                $params['message'] = end($result);
            }

        } else {
            //empty message, user is starting
            $params['message'] = null;
        }
        //end of this

        $user = UssdUser::wherePhone(substr($params['phone'], -12))->first();
        if(!$user){
            $user = self::createUser($params['phone']);
        }


        if($user){
            //check if user is confirmed
            if($params['message']=='reset'){
                self::resetUser($user);
                $output = 'Reset successful';
                self::sendResponse($output,2);
                exit;
            }

            if(($params['message'] == '') ||(strtolower(trim($params['message'])) == 'start')){
                self::resetUser($user);
            }

            if($user->confirmed == 0){
                $output = self::confirmUser($user,$params);
                exit;
            }
            //we have the user
            //self::sendResponse("USSD is working. Your number is ".$user->phone,1);
            //Switch based on user progress
            $type = 1;
            switch ($user->session) {

                case 0:
                    //Main Menu
                    $output = self::getMainMenu();
                    //update user status
                    $user->session = 2;
                    $user->progress = 1;
                    $user->step = 1;
                    $user->menu_item_id = 1;
                    $user->save();
                    break;
                case 1:
                    //user is taking the main menu
                    $output = self::confirmUser($user, $params);
                    break;
                case 2:
                    $output = self::ussdProgress($user, $params['message']);
                    //$output = self::MainMenuSelection($user, $params);
                    break;
                default :
                    //new user Main menu
                    $output = $this->mainMenu();
                    $type = 1;
                    break;
            }

            //progress 0
            self::sendResponse($output, $type);
            exit;

        }else{
            //createUser
            $user = self::createUser($params['phone']);
            //send mainmenu
            $output = self::getMainMenu();
            $user->session = 1;
            $user->progress = 1;
            $user->menu_item_id = 1;
            $user->save();
            //$output = "Please contact your HR to access Inukapap salary advance service";
            //self::sendSMS($params['phone'],$output);
            self::sendResponse($output,1);
            exit;
        }



        //stuff happening


        if($user->balance<0){
            self::sendResponse("You already have a loan",2);

        }else{

            self::sendmoney($user->phone,10);
            $user->balance = -10;
            $user->save();
        }
        self::sendResponse("USSD is working. Your number is ".$user->phone,1);
        //Switch based on user progress
        switch ($user->session) {

            case 0 :
                //Main Menu
                $output = self::getMainMenu();
                //update user status
                self::updateUserSession($user,1);
                self::updateUserProgress($user,1);

                break;
            case 1 :
                //reporting for the first time
                $output = self::ussdProgress($user,$params);
                break;
            case 3 :
                $output =  self::setPollingStation($user,$params);
                break;
            default :
                //new user Main menu
                $output = $this -> mainMenu();
                break;
            }

        //progress 0
        //TODO::Authenticate user based on request
//        self::sendResponse($output);
//        exit;


    }
    public function confirmUser($user,$params){
        switch ($user->progress) {

            case 0:
            //begin the process
                $user->session = 1;
                $user->progress = 1;
                $user->save();
//                $sacco =
                $output = "Welcome to {{Sacco name}}".PHP_EOL."Please enter your ID to confirm your registration";
                self::sendResponse($output,1);
                exit;
                break;
            case 1:
                //begin the process
                $user->session = 1;
                $user->progress = 2;
                $user->save();
                $output = "Please enter your PIN";
                self::sendResponse($output,1);
                exit;
                break;
            case 2:
                //begin the process
                $user->session = 1;
                $user->progress = 3;
                $user->save();
                $output = "Confirm your PIN";
                self::sendResponse($output,1);
                exit;
                break;
            case 3:
                //if(trim($params['message']) == trim($user->national_id)){
                    //confirm and send main menu
                    $output = self::getMainMenu();
                    $user->session = 2;
                    $user->progress = 1;
                    $user->menu_item_id = 1;
                    $user->confirmed = 1;
                    $user->save();
                    $message = "You are now registerd to Inukapap. Dial *384*2014# to access our services";
                    self::sendSMS($params['phone'],$message);
                    self::sendResponse($output,1);
                    exit;
//                }else{
//                    //error message
//                    if($user->step == 0) {
//                        $output = "Your ID did not match our records please retry";
//                        $user->step = 1;
//                        $user->save();
//                    }else{
//                        $output = "Your ID did not match our records, please contact your HR";
//                        self::resetUser($user);
//
//                    }
//                    self::sendResponse($output,2);
//                    exit;
//                }

                break;


        }

    }

    public function resetUser($user){
        $user->session = 0;
        $user->progress = 0;
        $user->menu_item_id=0;
        $user->step = 0;
        $user->save();
    }
    public function sendSMS($recipients,$message){

        $SmsController    = new SmsController();

        return $SmsController->sendSms($recipients,$message);
    }
    public function invalidResponse($user){

            $reply = "We could not understand your response";
            $output = self::getMainMenu();
            $user->session = 2;
            $user->progress = 1;
            $user->step = 0;
            $user->menu_item_id = 1;
            $user->save();

            $output =  $reply . PHP_EOL . $output;
            self::sendResponse($output,1);
            exit;

        }


        public function ussdProgress($user, $message) {
            $menu_item_id = $user -> menu_item_id;
            $step = $user -> step;
//            print_r($user);
//            exit;
            $menu = Ussdmenu::find($menu_item_id);
            //$menu_and_items = self::getMenuAndItems($menu_item_id);

            // print_r($menu);
            // exit;
            if (!$menu) {
                //reset user, we could not understand the response
                self::invalidResponse($user);

            }


            $menuType = $menu -> type;
//             print_r($menuType);
//             exit;
//             print_r($step);
//             exit;

            if ($menuType == 2) {

                //no verification but feed it response and update step as you move on
                if ($step > 0) {

                    $message = strtolower($message);
                    //feed the response to the table
                    // print_r($message);
                    // exit;
                    $valid = 1;
                    //$valid = $this -> validateResponse($menu -> id, $message, $step);
                    // print_r($valid);
                    // exit;
                    if ((is_array($valid)) && ($valid['valid'] == 0)) {

                        $output = $valid['message'];

                        //print_r($output);
                        return $output;

                    }

                    $dataa = array('user_id' => $user['id'], 'menu_id' => $menu_item_id, 'step' => $step-1, 'response' => $message);
                    //print_r($dataa);
                    //exit;

                    $result = UssdResponses::create($dataa);
//                    print_r($result);
//                    exit;
                   // $result = $Response_Model -> createResponse($dataa);

                    // print_r($result);
                    // exit;
                    if ($result) {
                        //check if we have another step, if we do, request, if we dont, compile the summary and confirm
                        $next_step = $step + 1;
//                         print_r($step);
//                         exit;
                       // $menuItemsModel = new Model_MenuItems();
                       // $menuItem = $menuItemsModel -> getNextMenuStep($menu_item_id, $next_step);
                        $menuItem = self::getNextMenuStep($menu_item_id,$step);

                        if (count($menuItem)>0) {
                            //update user data and next request and send back
                            $user->step = $next_step;
                            $user->save();


                            return $menuItem[0] -> description;

                        } else {
                            //compile summary for confirmation
                            //$menuItemsModel = new Model_MenuItems();
                            $menu_items = self::getMenuItems($menu_item_id);
//
//                            print_r($menu_items);
//                            exit;
                            //build up the responses
                            $confirmation = "Confirm?: " . $menu -> description;
                            foreach ($menu_items as $key => $value) {
//                                 print_r($value);
//                                 exit;
                                //print_r($confirmation);
                                //exit;
                                //select the corresponding response
                                if ($value->confirmation_phrase != 'PIN') {
                                    $phrase = $value->confirmation_phrase;
                                    //print_r($value[4]);
//                                    print_r($phrase);
//                                    exit;

                                    //$menu_item_id = $value['']
                                    $response = UssdResponses::whereUserIdAndMenuIdAndStep($user->id,$menu_item_id,$value->step)->get();
//                                    print_r($response);
//                                    exit;
                                    //$response = $Response_Model -> getResponse($user['id'], $menu_item_id, $value[3]);
                                    //print_r($response['response']."<br>");
//                                    if ($value[4] == 'Gender') {
//
//                                        if ($response['response'] == 1) {
//                                            $response['response'] = 'Male';
//
//                                        } elseif ($response['response'] == 2) {
//                                            $response['response'] = 'Female';
//
//                                        }
//
//                                    } elseif ($value[4] == 'Language') {
//
//                                        if ($response['response'] == 1) {
//                                            $response['response'] = 'English';
//
//                                        } elseif ($response['response'] == 2) {
//                                            $response['response'] = 'Kiswahili';
//
//                                        }
//
//                                    } elseif ($value[4] == 'Card') {
//                                        if ($response['response'] == 1) {
//                                            $response['response'] = 'Bebapay';
//
//                                        } elseif ($response['response'] == 2) {
//                                            $response['response'] = 'Abiria';
//
//                                        } elseif ($response['response'] == 3) {
//                                            $response['response'] = '1963';
//
//                                        }
//
//                                    }


                                    $confirmation = $confirmation . PHP_EOL . $value[4] . ": " . $response[0]->response;

                                    //print_r($response['response']);
                                    //exit;

                                }

                            }

//

                            if($menu->id == 3){

                                $response = UssdResponses::whereUserIdAndMenuIdAndStep($user->id,3,1)->orderBy('id','DESC')->first();
                               // $response = UssdResponses::whereMenuItemIdAndUserId($i, $user->id)->orderBy('id', 'DESC')->first();
                                $amount = $response->response;
                                //print_r($response->response);
                                //exit;

                            self::afterConfirmation($user,$menu->id,$amount);
                            exit;
                            }elseif($menu->id == 6 ){
                             $output = "Your Loan limit is Ksh. ".$user->loan_limit. ". Thanks for using Inukapap.";

                            }elseif($menu->id == 7){
                                $balance = -1*$user->balance;
                                $output = "Your current loan balance is Ksh ".$balance.". Thanks for using Inukapap.";
                            }elseif($menu->id == 2){
                                //$balance = -1*$user->balance;
                                $output = "To deposit, go to Mpesa Paybill, Enter Business no XXXXX, account no xxxx";
                            }else{
                                $output = "This service is currently not available. We will inform you once it is available";
                            }
                            self::resetUser($user);
                            self::sendResponse($output,2);
                            self::sendSMS($user->phone,$output);
                            exit;

                            //update the status to waiting for confirmation
                            $userModel = new Model_User();

                            $data = array('session' => 2, 'confirm_from' => $menu -> id);

                            //print_r($user['id']);
                            //exit;

                            $result = $userModel -> updateUserData($data, $user['id']);

                            //print_r($result);
                            //exit;
                            //$result = $userModel -> updateUserSession($user['id'],2);

                            return $confirmation . PHP_EOL . "1. Yes" . PHP_EOL . "2. No";
                            //exit;

                        }
                        //print_r($menuItem);
                        //exit;

                    }

                } else {

                    //when the user has not started the steps
                    $userModel = new Model_User();

                    $result = $userModel -> updateUserMenuStep($user['id'], 1);

                    $menuItemsModel = new Model_MenuItems();
                    $menuItem = $menuItemsModel -> getNextMenuStep($menu_item_id, 1);
                    return $menuItem -> description;
                    //exit;

                }

            } else {
                $selected_option = "";
                $next_menu_id = 0;
//                print_r($menu);
//                exit;
                //$menu_items = USSDMenuItems::whereMenuId($menu->id)->get();
                $menu_items = self::getMenuItems($menu->id);
//                print_r($menu_items);
//                exit;

                if (empty($menu_items)) {
                    $selected_option = $message;
                } else {

                    $message = strtolower($message);
//                    print_r($menu_items);
//                    exit;
                    foreach ($menu_items as $key => $value) {


                        if ((trim(strtolower($key)) == $message)) {
//                            print_r($value);
//                            exit;
                            //foreach ($value as $key1 => $value1) {
                            $selected_option = $key;
                            $next_menu_id = $value->next_menu_id;

                            //check for the type of menu_item_id
                            if ($value[5] == 3) {
                                //survey
                                $output = $this -> takeSurvey($user, $message);
                                return $output;

                            }
                        }

                        if (empty($selected_option)) {
                            foreach ($value as $key1 => $value1) {
                                //print_r($value1);
                                //exit;
                                if ((trim(strtolower($value1)) == $message)) {
                                    $selected_option = $value1;
                                }
                            }
                        }
                    }
                }

//                 print_r($next_menu_id);
//                 exit;

                if (empty($selected_option)) {
                    $selected_option = 0;
                }
                if (empty($next_menu_id) || ($next_menu_id<0)) {
                    $next_menu_id = 0;
                    self::resetUser($user);
                    $output = "This feature is not currently available, we will get back to you. Thanks for using Inukapap.";
                    self::sendSMS($user->phone,$output);
                    self::sendResponse($output,2);
                    exit;
                }
                //update the progress with the next menu id
                $user->menu_item_id = $next_menu_id;
                $result = $user->save();
                if ($result) {

                    $reply = "";
                    if ($next_menu_id == 0) {
                        self::invalidResponse($user);

                    }
                    $menu = Ussdmenu::find($next_menu_id);

                    if ($menu->type == 2) {
                        $output = $this -> singleProcess($menu->id, $user);

                        return $output;

                    } else {
                        //get the description
                        $description = $menu -> description;

                        //build up the options
                        $menuOptions = self::getMenuOptions($menu->id);
                        //$menuItemsModel = new Model_MenuItems();
                        //$menuOptions = $menuItemsModel -> getMenuOptions($menu_id);

                        return $description . PHP_EOL . $menuOptions;

                    }

                }

            }

        }
    public function afterConfirmation($user,$menu_id,$amount){


        switch ($menu_id) {
            case 3:
               //send this person money
               // $amount;
               $limit = $amount - $user->balance;
                if($limit>5000){
                    $output = "You are currently allowed to borrow a maximum of Ksh 5000. Please retry";
                    self::sendSMS($user->phone,$output);
                    exit;
                }


                $loan = $amount/100;
                $loan = $loan*92.5;
                $loan = floor($loan);

                self::sendmoney($user->phone,$loan);
               $output = "Your loan will be sent to your MPESA account shortly";
               self::resetUser($user);
               $user->balance = $user->balance-$amount;
               $user->save();

                //logtransaction
                self::logTransaction($user->id,1,$amount);
               $message = "You loan of Ksh ".$amount. " has been approved and will be disbursed to your MPESA shortly. It is due by end of this month";
               self::sendSMS($user->phone,$message);
               self::sendResponse($output,2);
               exit;

                break;
            case 4:

                break;
            default :
                $output = self::dataReview($user, $params);
                break;
        }

        return $output;

    }
    public function logTransaction($user_id,$type,$amount){
        //transaction type: 1 for loan request, 2 for loan repayment

        $dat = ['user_id' => $user_id, 'type_id'=>$type, 'amount' => $amount];
        return TransactionLogs::create($dat);

    }
    public function getNextMenuStep($menu_item_id,$step){
//        print_r($menu_item_id);
//        exit;

        return $next_menu_step = USSDMenuItems::whereMenuIdAndStep($menu_item_id,$step)->get();
//
//        if (empty($next_menu_step)) {
//            $row['id'] = 0;
//            return $row;
//        }else{
//            return $next_menu_step;
//        }
         }

    public function singleProcess($menu_item_id, $user) {

        //$menuItemsModel = new Model_MenuItems();
        //$menuItem = $menuItemsModel -> getNextMenuStep($menu_item_id, 1);

        $menuItem = self::getNextMenuStep($menu_item_id,1);
//        print_r($menuItem);
//        exit;
        if (count($menuItem)>0) {
            //update user data and next request and send back
//            print_r($user);
//            exit;
            $user->step=2;
            $user->save();
//        print_r($menuItem);
//            exit;
            return $menuItem[0] -> description;
            //exit;

        }

    }

    public function getMenuItems($id) {
        $result = USSDMenuItems::whereMenuId($id)->get();
//        print_r($result);
//        exit;
        $i = 1;
        $menuItems = array();
        foreach ($result as $key => $value) {
            $menuItems[$i] = $value;
            $i++;
        }
        return $menuItems;

    }
    public function sendUssdOutput($output,$type){
        if($type == 1){
            $response = "CON ";
            $response .= $output;
            echo $response;
        }elseif($type == 2){
            $response = "End ";
            $response .= $output;
            echo $response;
        }
        exit;

    }

    public function getMainMenu()
    {
        //return the home menu
        $menu = Ussdmenu::find(1);
//        print_r($menu);
//        exit;
        $menu_items = USSDMenuItems::whereMenuId($menu->id)->get();
//        print_r($menu_items);
//        exit;
        $i = 1;
        $output = "";
        foreach ($menu_items as $key => $value) {
            $output = $output . $i . ": " . $value->description . PHP_EOL;
            $i++;
        }

        return $menu->description.PHP_EOL.$output;
    }
    public function getMenuOptions($menu_id){
        $menu_items = USSDMenuItems::whereMenuId($menu_id)->get();
//        print_r($menu_items);
//        exit;
        $i = 1;
        $output = "";
        foreach ($menu_items as $key => $value) {
            $output = $output . $i . ": " . $value->description . PHP_EOL;
            $i++;
        }
        return $output;

    }
    public function getMenuAndItems($menu_id){
        $menu = Ussdmenu::find($menu_id);

        $menu_items = USSDMenuItems::whereMenuId($menu->id)->get();

        return array('menu'=>$menu,'menu_items' =>$menu_items);

    }

    public function testloan()
    {
        //Get user request
        $params = self::getRequestDetails();
//        print_r($params);
//        exit;
        $user = UssdUser::wherePhone($params['phone'])->first();
        if($user){

        }else{
            $user = self::createUser($params['phone']);
        }
        if($user->balance<0){
            self::sendResponse("Eish yawa, you were only to try the code once.",2);

        }else{

           // $status = json_decode(self::sendmoney($user->phone,10));

            $user->balance = -10;
            $user->save();
            self::sendResponse("Only a genius finds this hidden code. Ksh 10 has been sent to your MPESA",2);

        }
        self::sendResponse("USSD is working. Your number is ".$user->phone,1);
        exit;
    }
    public function sendmoney($mobile_no,$amount){
        $url = "https://lipisha.com/payments/accounts/index.php/v2/api/send_money";
        //$url = "http://developer.lipisha.com/index.php/v2/api/send_money";
        $api_key = "e1ebea542a1b4ef86189403b8951e3fd";
        //$api_key = "b8f3af58ade4c2ade71095106dbb50c8";
        $account_no = "02394";
        //$account_no = "02394";
        $mobile_no = $mobile_no;
        $amount = $amount;
        $api_signature = "2ypTpLBgjlBoZKvzjKtAIID8/e+AzrE8mDw8r46n1Zuv2kbayrFgPs6NwIkg4iQA+9t+43T/1y6D7pVs07BIyfV3dD+FCFeCAjRa5mZIcdHMblZFtIY3aJw2gowbg7sVUMk0Apg0I5EH56kl4Z4B9P2WtcAW8mKOqCjXss+6pCg=";
        //$api_signature = "xrvZJWxQQ5cx+ZQtivkiGvlUlvP9aD1JI4YNgQoCLgAgDPXLmXOXq6pvanzJQNQ/VX8GQes0eanqX3VCJFzmplvx7mkEZvUN0sx4Jq5XRPTPB8yVtR+IO+W22rUrSdk19ZA4wt3EPVkNRiA3z8dMkTDcEEXVkr15lci+wx4/vDI=";
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, 1);
       // curl_setopt($ch, CURLOPT_POSTFIELDS,
         //   "api_key=".$api_key."&api_signature=".$api_signature."&api_type=Callback&account_number=".$account_no."&mobile_number=".$mobile_no."&amount=".$amount);

// in real life you should use something like:
          curl_setopt($ch, CURLOPT_POSTFIELDS,
          http_build_query(array('api_key' => $api_key,
                                'api_signature'=> $api_signature,
                                'amount' =>$amount,
                                'api_type'=>'Callback',
                                'account_number'=> $account_no,
                                'mobile_number' => $mobile_no
          )));

// receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec ($ch);

        //print_r($server_output);


        curl_close ($ch);
        return $server_output;
        //var_dump($server_output);
        //echo "complete";
       // exit;
    }

    public function getRequestDetails()
    {
        $params = array();
        $params['sessionId']   = Input::get('sessionId');
        $params['serviceCode'] = Input::get("serviceCode");
        $params['phone'] = Input::get("phoneNumber");
       // $params['message']     = Input::get("text");

        $params['message'] = self::getLatestInput(Input::get("text"));

        //create log
        $dat = ['phone' => $params['phone'], 'text' => $_REQUEST['text']];
        UssdLogs::create($dat);
        return $params;
    }


    public function getLatestInput($input)
    {
        if (!empty($input)) {
            $message = $input;
            $result = explode("*", $message);
            if (empty($result)) {
                $message = $message;
            } else {
                end($result);
                $message = current($result);

            }
        } else {
            $message = 'start';
        }
        return $message;
    }


    public function sendResponse($output,$type)
    {
        if ($type==2) {
            $response = "END ";

        } else {
            $response = "CON ";
        }
        $response .= $output;
        header('Content-type: text/plain');
        echo $response;
        exit;
    }


    public function createUser($phone)
    {

        $dat = ['phone' => trim($phone),'national_id'=>12345, 'balance'=>0, 'loan_limit' => 10, 'username' => $phone, 'pin' => Hash::make(substr($phone, -4))];

        return UssdUser::create($dat);

    }

    public function ussd(){

    }

}