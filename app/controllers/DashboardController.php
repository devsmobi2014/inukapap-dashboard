<?php
/**
 * Created by PhpStorm.
 * User: guru
 * Date: 10/23/15
 * Time: 3:46 PM
 */
class DashboardController extends \BaseController {


    public function index(){

        $role=Auth::user()->role;

        if($role==1){
            $saccos=Sacco::all();
            $users = UssdUser::all();
            return View::make('dashboard.index',compact('saccos','users','role'));
        }else{

            $org_id=Auth::user()->org_id;

            return Redirect::intended('/sacco/view/'.$org_id);
        }


    }

}