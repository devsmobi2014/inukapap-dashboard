<?php

class SaccomemberController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /saccomember
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /saccomember/create
	 *
	 * @return Response
	 */
	public function create($id)
	{
		$sacco = Sacco::find($id);
		return View::make('members.create',compact('sacco'));

	}

	/**
	 * Store a newly created resource in storage.
	 * POST /saccomember
	 *
	 * @return Response
	 */
	public function storemember(){
		$input = Input::all();
		$input['username']=$input['phone'];

//		$validator=Validator::make(
//			array('first_name' => $input['first_name']),
//			array('first_name' => $input['last_name']),
//			array('phone' => $input['phone'])
//		);
//		if($validator->fails()){
//			return Redirect::back()->withErrors($validator)->withInput();
//		}

		$res=Saccomember::create($input);
		$sacco = Sacco::find($input['org_id']);
		require_once('SmsController.php');


		$SmsController    = new SmsController();

		$message = "Hi ".$input['first_name'].", your sacco ".$sacco->name." has signed you to Inukapap. Dial *384*2014# to complete the signup process";
		$result = $SmsController->sendSms($input['phone'],$message);

		return Redirect::to('sacco/view/'.$input['org_id']);

	}
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /saccomember/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /saccomember/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /saccomember/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /saccomember/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}