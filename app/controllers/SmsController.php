<?php

class SmsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /sms
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}
	public function sendSms($recipients,$message){
		require_once('AfricasTalkingGateway.php');

// Specify your login credentials
		$username   = "Inukapap";
		$apikey     = "c4e96c13de36631f34584e310caecc6a6464a9873df86aa4ecb23fb039351afa";

// Specify the numbers that you want to send to in a comma-separated list
// Please ensure you include the country code (+254 for Kenya in this case)
		//$recipients = "+254711XXXYYYZZZ,+254733XXXYYYZZZ";

// And of course we want our recipients to know what we really do
		//$message    = "I'm a lumberjack and its ok, I sleep all night and I work all day";

// Create a new instance of our awesome gateway class
		$gateway    = new AfricasTalkingGateway($username, $apikey);

// Any gateway errors will be captured by our custom Exception class below,
// so wrap the call in a try-catch block
		$from = "Inukapap_Ltd";
		try
		{
			// Thats it, hit send and we'll take care of the rest.
			$results = $gateway->sendMessage($recipients, $message,$from);
//			foreach($results as $result) {
//				// Note that only the Status "Success" means the message was sent
//				echo " Number: " .$result->number;
//				echo " Status: " .$result->status;
//				echo " MessageId: " .$result->messageId;
//				echo " Cost: "   .$result->cost."\n";
//			}
		}
		catch ( AfricasTalkingGatewayException $e )
		{
			echo "Encountered an error while sending: ".$e->getMessage();
		}
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /sms/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /sms
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /sms/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /sms/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /sms/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /sms/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}