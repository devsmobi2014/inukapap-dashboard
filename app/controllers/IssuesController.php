<?php

class IssuesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /issues
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$issues="";

		return View::make('issues.index',compact('issues'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /issues/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		$input= Input::all();
		$issue=new Issue();
		$issue->names=$input['names'];
		if($issue->save()){
			return Redirect::to('/issues')->with('success', 'Issue created successfully');
		}else{

		} return Redirect::to('/issues')->with('error','Error in issue creation');

	}

	/**
	 * Store a newly created resource in storage.
	 * POST /issues
	 *
	 * @return Response
	 */
	public function store()
	{
		//]
	}

	/**
	 * Display the specified resource.
	 * GET /issues/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /issues/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /issues/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /issues/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}