/**
 * Created by lawrence on 3/13/15.
 */

$("form#simlator").bind("keypress", function (e) {
    if (e.keyCode == 13) {
        return false;
    }
});

$('#sendMessage').click(function () {
    var Message = $("[name='message']").val();
    if (Message != '') {

        $.ajax({url: "/ussd?sessionId=123456&serviceCode=384&phoneNumber=254723384143&text=*384*"+Message,
            type: 'GET',
            dataType: 'text',
            success: function (data) {
//                var content = data.messages;
//                var str = '<pre>' + content[0].content + '</pre>';
                $('#emulator').empty();
                $('#emulator').html(data);
                $('[name="message"]').val("");
            },
            beforeSend: function () {
                //$('#emulator').addClass('spinner');
            },
            complete: function () {
                // $('#emulator').removeClass('spinner');
            }
        });

    } else {
        alert("Please enter message to send");
    }
});
